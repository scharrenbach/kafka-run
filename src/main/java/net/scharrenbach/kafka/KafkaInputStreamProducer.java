package net.scharrenbach.kafka;

/*
 * #%L
 * Kafka Run
 * %%
 * Copyright (C) 2013 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple producer for Kakfa that writes messages to stdout.
 * 
 * @author Thomas Scharrenbach
 * @version 0.8.0
 * @since 0.8.0
 * 
 */
public class KafkaInputStreamProducer<K, V> extends Thread {

	private static final Logger _log = LoggerFactory
			.getLogger(KafkaInputStreamProducer.class);

	//
	//
	//

	private KafkaProducer<K, V> _producer;

	private LineParser<K, V> _parser;

	private InputStream _inputStream;

	//
	//
	//

	/**
	 * <p>
	 * 
	 * </p>
	 * 
	 * 
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 */
	public KafkaInputStreamProducer() {
		super();
		_producer = new KafkaProducer<K, V>();
		_inputStream = System.in;
	}

	//
	//
	//

	@Override
	public void run() {
		if (_parser == null) {
			throw new NullPointerException();
		}

		_producer.init();
		final BufferedReader reader = new BufferedReader(new InputStreamReader(
				_inputStream));

		//super.run();
		try {
			String line = reader.readLine();
			while ((!isInterrupted()) && line != null) {
				_producer.write(_parser.parse(line));
				_log.debug("Writing item");
				line = reader.readLine();
			}
		} catch (IOException e) {
			_log.error("Error reading from stream: ", e);
			interrupt();
		}
		_log.debug("Started shutting down producer...");
		_producer.shutdown();
		_log.info("Finished shutting down producer.");
	}

	public void setParser(LineParser<K, V> parser) {
		_parser = parser;
	}

	public LineParser<K, V> getParser() {
		return _parser;
	}

	public void setInputStream(InputStream inputStream) {
		_inputStream = inputStream;
	}

	public InputStream getInputStream() {
		return _inputStream;
	}

	public void setTopic(String topic) {
		_producer.setTopic(topic);
	}

	public String getTopic() {
		return _producer.getTopic();
	}

}
