package net.scharrenbach.kafka;

import java.util.Properties;

/*
 * #%L
 * Kafka Run
 * %%
 * Copyright (C) 2013 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/**
 * <p>
 * Keys and default values for the properties used in Kafka.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.0.8
 * @since 0.0.8
 * 
 */
public class KafkaProperties {

	public static final String METADATA_BROKER_LIST = "metadata.broker.list";
	public static final String DEFAULT_METADATA_BROKER_LIST = "localhost:9092";

	public static final String SERIALIZER_CLASS = "serializer.class";
	public static final String DEFAULT_SERIALIZER_CLASS = "kafka.serializer.StringEncoder";

	public static final String PARTITIONIER_CLASS = "partitioner.class";
	public static final String DEFAULT_PARTITIONIER_CLASS = "net.scharrenbach.kafka.HashModPartitioner";

	public static final String REQUEST_REQUIRED_ACKS = "request.required.acks";
	public static final String DEFAULT_REQUEST_REQUIRED_ACKS = "1";

	public static final String ZOOKEEPER_CONNECT = "zookeeper.connect";
	public static final String DEFAULT_ZOOKEEPER_CONNECT = "localhost:2181";

	public static final String GROUP_ID = "group.id";
	public static final String DEFAULT_GROUP_ID = "default";

	public static final String ZOOKEEPER_SESSION_TIMEOUT_MS = "zookeeper.session.timeout.ms";
	public static final String DEFAULT_ZOOKEEPER_SESSION_TIMEOUT_MS = "400";

	public static final String ZOOKEEPER_SYNC_TIME_MS = "zookeeper.sync.time.ms";
	public static final String DEFAULT_ZOOKEEPER_SYNC_TIME_MS = "200";

	public static final String AUTO_COMMIT_INTERVAL_MS = "auto.commit.interval.ms";
	public static final String DEFAULT_AUTO_COMMIT_INTERVAL_MS = "1000";

	public static final String DEFAULT_TOPIC = "default";

	//
	//
	//

	/**
	 * <p>
	 * Create a fresh {@link Properties} object as defined in the <a href=
	 * "https://cwiki.apache.org/confluence/display/KAFKA/Consumer+Group+Example"
	 * >Kafka high-level consumer API documentation</a>.
	 * </p>
	 * 
	 * @return fresh {@link Properties} object.
	 */
	public static final Properties createConsumerDefaultProperties() {
		final Properties properties = new Properties();
		properties.put(ZOOKEEPER_CONNECT, DEFAULT_ZOOKEEPER_CONNECT);
		properties.put(GROUP_ID, DEFAULT_GROUP_ID);
		properties.put(ZOOKEEPER_SESSION_TIMEOUT_MS,
				DEFAULT_ZOOKEEPER_SESSION_TIMEOUT_MS);
		properties.put(ZOOKEEPER_SYNC_TIME_MS, DEFAULT_ZOOKEEPER_SYNC_TIME_MS);
		properties
				.put(AUTO_COMMIT_INTERVAL_MS, DEFAULT_AUTO_COMMIT_INTERVAL_MS);
		return properties;
	}

	/**
	 * <p>
	 * Create a fresh {@link Properties} object as defined in the <a href=
	 * "https://cwiki.apache.org/confluence/display/KAFKA/0.8.0+Producer+Example"
	 * >Kafka high-level producer API documentation</a>.
	 * </p>
	 * 
	 * @return fresh {@link Properties} object.
	 */
	public static final Properties createProducerDefaultProperties() {
		final Properties properties = new Properties();
		properties.put(METADATA_BROKER_LIST, DEFAULT_METADATA_BROKER_LIST);
		properties.put(SERIALIZER_CLASS, DEFAULT_SERIALIZER_CLASS);
		properties.put(PARTITIONIER_CLASS, DEFAULT_PARTITIONIER_CLASS);
		properties.put(REQUEST_REQUIRED_ACKS, DEFAULT_REQUEST_REQUIRED_ACKS);
		return properties;
	}

}
