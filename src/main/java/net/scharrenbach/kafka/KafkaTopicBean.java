package net.scharrenbach.kafka;

/*
 * #%L
 * Kafka Run
 * %%
 * Copyright (C) 2013 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import kafka.common.Topic;

/**
 * <p>
 * Bean holding all information necessary for a Kafka {@link Topic}.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * @version 0.8.0
 * @since 0.8.0
 * 
 */
public class KafkaTopicBean {

	private static final String DEFAULT_TOPIC = "default";
	private static final int DEFAULT_N_PARTITIONS = 1;
	private static final int DEFAULT_REPLICATION_FACTOR = 1;
	private static final String DEFAULT_REPLICA_ASSIGNMENT_STR = "";

	private String _topic;
	private int _nPartitions;
	private int _replicationFactor;
	private String _replicaAssignmentStr;

	public KafkaTopicBean(String topic) {
		setTopic(topic);
		setNPartitions(DEFAULT_N_PARTITIONS);
		setReplicationFactor(DEFAULT_REPLICATION_FACTOR);
		setReplicaAssignmentStr(DEFAULT_REPLICA_ASSIGNMENT_STR);
	}

	public KafkaTopicBean() {
		this(DEFAULT_TOPIC);
	}

	public KafkaTopicBean(String topic, int nPartitions, int replicationFactor,
			String replicaAssignmentStr) {
		setTopic(topic);
		setNPartitions(nPartitions);
		setReplicationFactor(replicationFactor);
		setReplicaAssignmentStr(replicaAssignmentStr);
	}

	@Override
	public String toString() {
		return _topic;
	}

	@Override
	public int hashCode() {
		return _topic.hashCode();
	}

	public void setTopic(String topic) {
		if (topic == null) {
			throw new NullPointerException(String.format(
					"Parameter %s must not be null!", "topic"));
		}
		_topic = topic;
	}

	public String getTopic() {
		return _topic;
	}

	public void setNPartitions(int nPartitions) {
		if (nPartitions < 1) {
			throw new IllegalArgumentException(
					"Number of partitions must at least be one!");
		}
		_nPartitions = nPartitions;
	}

	public int getNPartitions() {
		return _nPartitions;
	}

	public void setReplicationFactor(int replicationFactor) {
		if (replicationFactor < 1) {
			throw new IllegalArgumentException(
					"Replication factor must at least be one!");
		}
		_replicationFactor = replicationFactor;
	}

	public int getReplicationFactor() {
		return _replicationFactor;
	}

	public void setReplicaAssignmentStr(String replicaAssignmentStr) {
		if (replicaAssignmentStr == null) {
			throw new NullPointerException(String.format(
					"Parameter %s must not be null!", "replicaAssignmentStr"));
		}
		_replicaAssignmentStr = replicaAssignmentStr;
	}

	public String getReplicaAssignmentStr() {
		return _replicaAssignmentStr;
	}

}
