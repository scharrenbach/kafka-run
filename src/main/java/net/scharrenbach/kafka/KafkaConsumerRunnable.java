package net.scharrenbach.kafka;

import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.message.MessageAndMetadata;

/**
 * This {@link Runnable} processes messages from a {@link KafkaStream} using the
 * {@link MessageAndMetadata} iterator.
 * 
 * @author Thomas Scharrenbach
 * @version 0.8.0
 * @since 0.8.0
 * 
 */
public class KafkaConsumerRunnable<K, V> implements Runnable {

	private final KafkaStream<K, V> _kafkaStream;

	private final KafkaMessageProcessorFactory<K, V> _kafkaMessageProcessorFactory;

	private boolean _interrupted;

	/**
	 * 
	 * @param kafkaStream
	 * 
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 */
	public KafkaConsumerRunnable(KafkaStream<K, V> kafkaStream,
			KafkaMessageProcessorFactory<K, V> kafkaMessageProcessorFactory) {
		if (kafkaStream == null) {
			throw new NullPointerException();
		}
		if (kafkaMessageProcessorFactory == null) {
			throw new NullPointerException();
		}
		_kafkaStream = kafkaStream;
		_kafkaMessageProcessorFactory = kafkaMessageProcessorFactory;
	}

	/**
	 * 
	 * @author Thomas Scharrenbach
	 * @version 0.8.0
	 * @since 0.8.0
	 */
	@Override
	public void run() {
		final ConsumerIterator<K, V> kafkaIt = _kafkaStream.iterator();
		final KafkaMessageProcessor<K, V> kafkaMessageProcessor = _kafkaMessageProcessorFactory
				.createKafkaMessageProcessor();
		while ((!isInterrupted()) && kafkaIt.hasNext()) {
			final MessageAndMetadata<K, V> item = kafkaIt.next();
			kafkaMessageProcessor.processKafkaMessage(item);
		}
		System.out.println("Stream finished");
	}

	/**
	 * 
	 * @param interrupted
	 * 
	 * @version 0.8.0
	 * @since 0.8.0
	 */
	public void setInterrupted(boolean interrupted) {
		_interrupted = interrupted;
	}

	/**
	 * 
	 * @return
	 * 
	 * @version 0.8.0
	 * @since 0.8.0
	 */
	public boolean isInterrupted() {
		return _interrupted;
	}

}
