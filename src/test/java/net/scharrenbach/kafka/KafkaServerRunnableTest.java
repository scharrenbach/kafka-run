package net.scharrenbach.kafka;

/*
 * #%L
 * Kafka Run
 * %%
 * Copyright (C) 2013 Thomas Scharrenbach
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import net.scharrenbach.zookeeper.test.ZooKeeperServerRunnable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * Unit test for {@link KafkaServerRunnable}.
 * 
 * @author Thomas Scharrenbach
 * @version 0.8.0
 * @since 0.8.0
 * 
 */
public class KafkaServerRunnableTest {

	private static final Logger _log = LoggerFactory
			.getLogger(KafkaServerRunnableTest.class);

	@Parameters({ "zookeeper-config", "kafka-config" })
	@Test
	public void test(
			@Optional("src/test/resources/config/zoo.cfg") String zooKeeperConfig,
			@Optional("src/test/resources/config/kafka-server.properties") String kafkaConfigFile) {
		try {
			final File zooKeeperDataDir = new File("zookeeper-data");
			if (zooKeeperDataDir.exists() && zooKeeperDataDir.isDirectory()) {
				_log.info("Deleting existing ZooKeeper directory {}",
						zooKeeperDataDir);
				delete(zooKeeperDataDir);
			}
			final File kafkaDataDir = new File("kafka-data");
			if (kafkaDataDir.exists() && kafkaDataDir.isDirectory()) {
				_log.info("Deleting existing Kafka directory {}", kafkaDataDir);
				delete(kafkaDataDir);
			}

			assert (!zooKeeperDataDir.exists() && !kafkaDataDir.exists());

			// Note: The Kafka Runnable must be created before the ZooKeeper
			// Runnable when using shutdown hooks.
			final KafkaServerRunnable kafkaServerRunnable = KafkaServerRunnable
					.newInstance(kafkaConfigFile, true);
			final ZooKeeperServerRunnable zooKeeperServerRunnable = ZooKeeperServerRunnable
					.newInstance(zooKeeperConfig, true);

			final Thread kafkaThread = new Thread(kafkaServerRunnable);
			final Thread zkThread = new Thread(zooKeeperServerRunnable);
			zkThread.start();
			Thread.sleep(1000);
			kafkaThread.start();
			Thread.sleep(1000);

			final KafkaTopicBean testTopicOne = new KafkaTopicBean("testOne");
			kafkaServerRunnable.addTopic(testTopicOne);
			final KafkaTopicBean testTopicTwo = new KafkaTopicBean("testTwo");
			kafkaServerRunnable.addTopic(testTopicTwo);

			Thread.sleep(200);

		} catch (Exception e) {
			_log.error("Error during test!", e);
			assert false;
		}
	}

	/**
	 * Taken from http://stackoverflow.com/questions/779519/delete-files-recursively-in-java
	 * @param file
	 * @throws IOException
	 */
	private void delete(File file) throws IOException {
		if (file.isDirectory()) {
			for (File content : file.listFiles())
				delete(content);
		}
		if (!file.delete())
			throw new FileNotFoundException("Failed to delete file: " + file);
	}
}
